\section{Aggiornamento}
Prima dell'effettivo sviluppo di una procedura di aggiornamento è opportuno chiarire quali siano i requisiti necessari e quali gli aspetti critici.
\\
Innanzitutto una generica metodologia di aggiornamento comprende le seguenti fasi:
\begin{enumerate}
	\item
	      Ottenere l’aggiornamento,
	\item
	      Verificarne integrità e autenticità,
	\item
	      Se possibile preoccuparsi della conservazione dei dati utente,
	\item
	      Eseguirlo.
\end{enumerate}
~\\
Per la valutazione di una procedura di aggiornamento occorre considerare, oltre alla coerenza con i passaggi precedentemente citati, anche la probabilità di successo (o di recupero da errori) e la semplicità dell’operazione dal punto di vista dell’utente finale.
\\
Quest’ultima è fortemente correlata alla richiesta di intervento manuale. È perciò evidente la possibilità di ottimizzare questo aspetto distribuendo il compito di effettuare le varie fasi al CoderBot piuttosto che all’utente. Ciò incide positivamente anche sulla probabilità di successo in quanto l’utente non è necessariamente provvisto delle competenze e della famigliarità utili alle operazioni richieste e potrebbe commettere errori.

\subsection{Sicurezza}
I processi di aggiornamento rappresentano aspetti critici per quanto riguarda la sicurezza di un sistema.
\\
Spesso infatti contengono programmi che verranno successivamente eseguiti e nel caso non siano state  prese le opportune precauzioni, possono essere utilizzati come vettori per l'iniezione di codice malevolo.
\\
Gli aggiornamenti inoltre sono generalmente ottenuti per mezzo di internet, per cui trascurare le necessarie accortezze espone a molteplici rischi.
\\
Per trattare di come gestire la sicurezza di una metodologia di aggiornamento è utile chiarire alcuni concetti.

\subsubsection{CIA Triad}
Per lo sviluppo di applicazioni sicure oppure per individuare eventuali problemi di progettazione è necessario proteggere alcune proprietà fondamentali riguardanti i dati scambiati:
\begin{enumerate}
	\item
	      La confidenzialità, ossia l’impossibilità di accedere all’informazione in questione da parte di terzi.
	\item
	      L'integrità, cioè la certezza che il dato non abbia subito modifiche
	      durante lo scambio.
	\item
	      La disponibilità intesa come la garanzia che l’informazione sia accessibile quando necessario.
	\item
	      L'autenticazione e il non ripudio, ovvero la certezza di poter stabilire chi è l’autore del dato sotto esame.
\end{enumerate}
~\\
Questi principi vengono spesso citati come CIA Triad (Confidentiality, Integrity, Availability) \cite{cia:1}.

\subsubsection{Crittografia}
La crittografia è la disciplina che si occupa di rendere una comunicazione sicura di fronte a soggetti non autorizzati, rispettando quindi i principi precedentemente citati.
\\
Le varie tipologie di crittografia condividono alcune somiglianze \cite{crypto_rivest:1}:
\begin{itemize}
	\item
	      L’obiettivo è quello di scambiare un messaggio “sensibile“ in modo “sicuro”, rispettando le proprietà CIA (non necessariamente tutte).
	\item
	      La presenza di un messaggio cifrato la cui intercettazione da parte di terzi non violi la sicurezza del messaggio “sensibile”.
	\item
	      In genere è presente una chiave spesso sotto forma di parola o file.
	\item
	      Un algoritmo di cifratura permette di ottenere il messaggio cifrato a partire da quello sensibile e dall’eventuale chiave.
	\item
	      Un algoritmo di decifratura a partire dal messaggio cifrato e dalla chiave permette al ricevente di ottenere le informazioni desiderate.
\end{itemize}

\paragraph{Crittografia a Chiave Simmetrica}
Si parla di crittografia a chiave simmetrica quando i due soggetti (mittente e ricevente) sono i soli a conoscenza di una stessa chiave “segreta” precedentemente scambiata in condizioni sicure. 
\\
L’informazione viene cifrata dal mittente, condivisa in forma criptata e poi decifrata dal ricevente utilizzando la stessa chiave, garantendo contemporaneamente confidenzialità, integrità e autenticazione, difatti solo chi è in possesso della chiave ha la facoltà di cifrare o decifrare il messaggio.
\\
Questo sistema lamenta tuttavia l’impossibilità di garantire una delle proprietà indipendentemente dalle altre. Inoltre lo scambio della chiave segreta rimane un aspetto critico, rendendo complicato per esempio comunicare in maniera sicura con un nuovo contatto.
\\
Per citare alcuni tra gli algoritmi di crittografia simmetrica più utilizzati: AES (Advanced Encryption Standard) e DES (Data encryption Standard) \cite{crypto_rfc_2828:1}.

\paragraph{Crittografia a Chiave Asimmetrica}
La crittografia a chiave asimmetrica, detta anche crittografia a chiave pubblica, è caratterizzata dalla presenza di una coppia di chiavi, dette pubblica e privata; la seconda è pensata per essere custodita segretamente, mentre la prima per essere condivisa senza preoccupazioni sulla sua intercettazione da parte di terzi.
\\
Ogni messaggio cifrato utilizzando una delle due è decifrabile da chiunque conosca la seconda chiave della coppia.
In questo modo a differenza della cifratura a chiave simmetrica è possibile garantire delle proprietà citate in precedenza quelle richieste dalla situazione.
\\
Consideriamo due interlocutori, ognuno dei quali è il solo a conoscenza della propria chiave privata, mentre entrambi conoscono la chiave pubblica propria e altrui. Il mittente cifrando il messaggio attraverso la chiave pubblica del ricevente è sicuro che quest’ultimo sia l’unico in grado di decifrarlo (confidenzialità). Se invece il messaggio viene cifrato utilizzando la propria chiave privata, il ricevente ha la facoltà di verificarne l’effettiva origine (autenticazione e non ripudio, integrità). Quando si parla di messaggi firmati o di firme digitali ci si riferisce a questo procedimento.
\\
Esiste inoltre la possibilità di combinare i due usi appena descritti in modo da garantire contemporaneamente più proprietà.
\\
Alcuni esempi di implementazione di algoritmi di crittografia asimmetrica: RSA (Rivest-Shamir-Adleman), ECC (Elliptic Curve Cryptography).
\\
Essendo questi algoritmi computazionalmente pesanti vengono solitamente utilizzati per la condivisione in sicurezza di una chiave simmetrica (spesso casuale) per poi continuare la comunicazione con algoritmi di crittografia simmetrica.
Troviamo un'implementazione simile per esempio nel protocollo TLS (Transport Layer Security) \cite{crypto_rfc_2828:1}.

\subsubsection{Checksum}
Un valore di dimensioni limitate generato da un dato potenzialmente ampio, per esempio un file, è detto checksum o \textit{“message digest”}. Viene in genere utilizzato come controllo (da check) dell’integrità di un dato ricevuto confrontando il checksum di quest’ultimo con quello che si suppone generato dall’originale.
\\
MD5 (Message Digest) è un algoritmo largamente utilizzato nel generare checksum \cite{crypto_rfc_2828:1}.

\paragraph{Algoritmi di Hash}
Con algoritmo di hash si indica un funzione che associa valori in un dominio potenzialmente ampio ad un codominio limitato.
\\
Sono effettivamente utilizzati per generare checksum a partire da messaggi generici o file.
\\
Le funzioni di hash che si prestano ad essere utilizzate nella crittografia (algoritmi crittografici di hash) sono quelli definibili \textit{“one-way”}, deve cioè essere computazionalmente impossibile risalire al messaggio originale a partire dal checksum. Inoltre con un buon algoritmo crittografico di hash è altamente improbabile che di fronte ad una modifica dell’input il checksum rimanga invariato.
\\
Spesso per garantire l’integrità di un messaggio generico è il checksum (di lunghezza limitata) ad essere firmato, così da evitare operazioni di crittografia asimmetrica su dati di larghe dimensioni.
\\
Sono esempi di algoritmi crittografici di hash quelli appartenenti alle famiglie SHA-2 e SHA-3 (Secure Hash Algorithm) \cite{crypto_rfc_2828:1}.


\subsubsection{OpenPGP}
OpenPGP consiste in un protocollo libero basato sul software PGP (Pretty Good Privacy), nato con lo scopo di garantire la comunicazioni sicure oltre che a fornire uno strumento in grado di gestire chiavi asimmetriche e firme digitali \cite{openpgp:1}.
\\
GnuPG, GNU Privacy Guard o più semplicemente gpg, è un’implementazione libera dello standard, distribuita secondo la licenza GNU GPL \cite{gpg_man:1}.

\subsubsection{Utenti e Permessi}
Nei sistemi Linux o più in generale in quelli Unix le gestione utenti  ha il fine di garantire sicurezza limitandone i privilegi.
\\
Si parla di superutente o utente root per indicare l’utente privilegiato che si occupa della gestione del sistema: viene solitamente utilizzato per configurare, installare o rimuovere software, oppure per gestire i privilegi degli altri utenti \cite{root_tldp:1}.
\\
La gestione degli accessi alle varie risorse è normalmente di tipo DAC (Discretionary access control), in quanto ogni utente che detiene il permesso di accedere ad un elemento ha la facoltà di concederlo ad altri \cite{dac:1}.
\\
Ogni file appartiene ad un utente e ad un gruppo di utenti ed è possibile specificare quali permessi (lettura, scrittura ed esecuzione) spettano al proprietario, agli utenti che appartengono al gruppo e a tutti gli altri \cite{files_permission_tldp:1}.

\subsection{Archivi e Compressione}
Spesso gli aggiornamenti sono disposti all’interno di archivi generalmente compressi. Questo perché i file necessari sono potenzialmente numerosi e di dimensioni notevoli, e distribuirli separatamente potrebbe generare complicazioni.

\subsubsection{File Archivio}
Un file di archivio come suggerito dal nome si presenta sotto forma di un unico elemento che permette di collocare al suo interno più file o cartelle.

\paragraph{TAR}
Tape ARchive è un formato di file archivio creato inizialmente per sistemi UNIX, successivamente standardizzata da POSIX.
\\
TAR è anche il nome dell’applicazione generalmente utilizzata per gestire questi file \cite{tar_7z:1}.

\subsubsection{Compressione dati}
La compressione dei dati è una tecnica utilizzata per codificare informazioni utilizzando un numero di bit minore rispetto all’originale.
\\
Si distingue in \textit{“lossy”} e \textit{“lossless”}, la prima adempie allo scopo rimuovendo dati relativamente poco importanti, mentre la seconda minimizzando la ridondanza, come per esempio byte ripetuti, senza quindi perdita di informazione.

\paragraph{Xz Utils}
È il nome di un software libero per la compressione dati lossless attraverso il protocollo LZMA2 (Lempel–Ziv–Markov chain algorithm, dai nomi dei creatori e delle tecnologie impiegate) \cite{xz_utils:1}.
\\
Il formato xz permette di comprimere e contenere singoli file; non ha quindi funzionalità di archivio.

\subsection{Ripristino della MicroSD}
Risiedendo l’intera memoria del CoderBot su microSD è possibile aggiornare il software agendo direttamente su di essa.
\\
Ciò richiede di accedere fisicamente al dispositivo per rimuovere la scheda di memoria e di installarvi un aggiornamento, precedentemente scaricato, attraverso un Personal Computer, risultando in una procedura scomoda oltre che dipendente dalla disponibilità di una porta o un adattatore microSD, di specifici software installati su PC e di una certa dimestichezza nell’utilizzo di quest’ultimi.
\\
Pecca inoltre nella conservazione dei dati utente e nella verifica di integrità e autenticità, entrambe effettuabili solo manualmente.
\\
É quindi evidente una carenza di semplicità dovuta alla notevole richiesta di intervento umano, sin dall’ottenimento dell’aggiornamento fino a verifica, applicazione e gestione dei dati.
\\
Per tali motivi una procedura del genere è da prendere in considerazione soltanto in seguito a malfunzionamenti irrisolvibili diversamente.

\subsection{Package Manager}
Installazione, aggiornamento e rimozione dei vari software in ambiente Linux sono operazioni generalmente effettuate dai cosiddetti gestori dei pacchetti (package manager). Questi interrogano vari repository permettendo di cercare nuovi software e di controllare l’esistenza di versioni più aggiornate di quelli presenti sul sistema.
\\
Per pacchetto si intende un archivio generalmente compresso al cui interno sono presenti tutte le risorse necessarie ad un determinato software, come eseguibili, codici sorgente e file di configurazione.
\\
Mentre un repository è una risorsa, spesso online, da dove è possibile ottenere vari pacchetti.

\paragraph{APT}
In Raspbian, così come in Debian, la gestione pacchetti è affidata ad APT (Advanced Package Tool): software che si occupa principalmente di ottenere pacchetti da repository \cite{apt:1} e successivamente utilizza Dpkg (Debian Package), il quale ne svolge effettivamente l'installazione, la configurazione o la rimozione \cite{dpkg_org:1}.
\\
Tuttavia CoderBot non è necessariamente provvisto di connessione a internet, perciò non è possibile utilizzare repository online. Utilizzare APT con repository locali, non risolve il problema, ma lo devia verso l’aggiornamento di questi.
\\
In più CoderBot richiede maggiore versatilità rispetto a quella garantita da APT: è indispensabile poter aggiornare alcune sue dipendenze non disponibili in forma di pacchetti Debian, così come modificare l'applicazione Python stessa, oppure le configurazioni di sistema.

\subsection{Implementazione}
È necessaria una procedura di aggiornamento flessibile che consenta a CoderBot di ottenere l’aggiornamento anche senza essere connesso direttamente ad internet e che permetta varie operazioni sul sistema, senza che si limiti ai pacchetti Raspbian.
\\
Per massimizzare la semplicità occorre affidare al dispositivo tutte le fasi che è in grado di svolgere, minimizzando l’intervento umano.

\subsubsection{Ottenimento}
Non essendo il CoderBot necessariamente connesso alla rete è fondamentale poter fornire l’aggiornamento al dispositivo tramite modalità alternative. Da evitare sono quelle che dipendono da un accesso fisico al Raspberry Pi, in quanto non offrono considerevoli vantaggi in semplicità rispetto al ripristino della microSD.
\\
Il metodo scelto è il download di un pacchetto online su un dispositivo dell’utente, per poi effettuare l’aggiornamento attraverso l’interfaccia web del CoderBot una volta connessi a quest’ultimo.
\\
In questo modo i requisiti vengono limitati ad un generico dispositivo dotato di Web Browser e di rete WiFi.
\\
Per evitare che si generino complessità e confusione è opportuno che l’aggiornamento si presenti come un unico file, in questo caso si tratta di un archivio Tar, compresso attraverso Xz così da ottimizzarne le dimensioni.

\subsubsection{Verifica}
La verifica di autenticità ed integrità del pacchetto è affidata GPG e svolta interamente sul dispositivo. Ciò richiede che questo conosca la chiave pubblica CoderBot, che può essere inizialmente installata dal produttore, e che il pacchetto sia firmato con la chiave privata corrispondente.
\\
Tuttavia firmare e verificare l’intero pacchetto potenzialmente di larghe dimensioni è un inutile spreco di risorse: esiste la possibilità di firmare un suo checksum e successivamente verificarlo e controllare se il pacchetto corrisponde.
\\
GPG integra le funzioni di generazione e di verifica di un file di signature separato \cite{gpg_man:1}.
\\
Ciò comporta la presenza di un altro file oltre al pacchetto di aggiornamento, complicanza facilmente risolta archiviando ulteriormente i due file via Tar.
\\
Perciò l’archivio scaricato racchiude al suo interno il pacchetto compresso effettivamente necessario all’aggiornamento e il file di firma che ne permette la verifica.

\subsubsection{Esecuzione e Gestione dei Dati Utente}
All’interno del pacchetto compresso sono presenti:
\begin{enumerate}
	\item
	      Un file informativo chiamato \textit{"update.info"},
	\item
	      Una cartella chiamata \textit{"files"},
	\item
	      Una chiamata \textit{"scripts"}.
\end{enumerate}
~\\
Il primo contiene informazioni utili all’utente per comprendere in cosa consista l’aggiornamento, come la descrizione delle modifiche che stanno per essere effettuate, ed al dispositivo; riporta il nuovo numero di versione insieme a funzioni con il compito di verificare l’idoneità del sistema. Queste possono semplicemente limitarsi a controllare se il numero di versione attuale figuri in quelli compatibili, oppure effettuare verifiche più approfondite conformi alle necessità dell’aggiornamento.
\\
La cartella scripts contiene i programmi che effettivamente svolgono le operazioni necessarie all’aggiornamento, mentre files contiene i file o le cartelle richiesti dai vari script. Una volta superati i controlli, questi ultimi sono eseguiti uno ad uno in ordine alfabetico direttamente dal dispositivo.
\\
In questo modo viene garantita la versatilità richiesta da CoderBot: in base alle esigenze dell’aggiornamento si è in grado di decidere quali script inserire e quali no. Posizionando il necessario in files è possibile utilizzare script che installano o aggiornano pacchetti Raspbian attraverso dpkg, così come moduli Python. Oppure possono essere creati appositi script per copiare, rimuovere o modificare file di configurazione e per svolgere operazioni di backup, ripristino o adattamento dei vari dati utente.
\\
%%disegnino di udate.info scripts files
\begin{figure}[h!]
    \centering
	\framebox[0.5\textwidth]{%
		\begin{minipage}{0.4\textwidth}
			\dirtree{%
				.1 update.tar.
				.2 update.sig.
				.2 update.tar.xz.
				.3 files.
				.4 file1.
				.4 folder1.
				.5 file2.
				.5 folder2.
				.6 file3.
				.3 scripts.
				.4 01\_script.sh.
				.4 02\_script.sh.
				.3 update.info.
			}
		\end{minipage}
	}
	\caption{Struttura del Pacchetto di aggiornamento}
\end{figure}
~\\
L’esecuzione degli script di aggiornamento deve essere svolta garantendo loro i privilegi di amministratore, necessari per esempio per le operazioni sui pacchetti effettuate da dpkg e per la modifica di configurazioni di sistema.
\\
Ciò comporta certamente dei rischi, tuttavia una volta superata la verifica di autenticità e integrità si ha la certezza che i programmi utilizzati sono sviluppati da CoderBot, a patto che la chiave pubblica contenuta nel dispositivo sia corretta.
\\
Poiché per modificare la chiave sono necessari i permessi di root, è privo di senso considerare la procedura di aggiornamento un modo di eseguire codice malevolo in ambiente privilegiato.
\\
Considerando che la modifica di programmi in esecuzione potrebbe causare comportamenti imprevisti è consigliabile effettuare l'aggiornamento prima che le varie applicazioni del CoderBot vengano avviate. Ciò è possibile salvando il pacchetto in una posizione prefissata e riavviando in seguito il sistema che deve controllarne l'esistenza ed eseguire l'aggiornamento, prima dell'inizializzazione dei vari software.