# Aggiornamento e Ripristino delle Funzionalità di Sistemi Linux Embedded come CoderBot

Needed tex packages:
```
wrapfig
babel-italian
dirtree
```

## Build pdf

```
pdflatex Tesi
bibtex Tesi
pdflatex Tesi
```

## Pre-built pdf
[Tesi.pdf](Tesi.pdf)

## Presentation
[Presentazione.pdf](Presentazione.pdf)