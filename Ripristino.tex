\section{Ripristino}
\subsection{Concetti utili}
A seguire alcuni concetti utili alla comprensione.

\subsubsection{Partizione}
Parlando di dispositivi di archiviazione è uso comune dividerne i dati in più partizioni (dal latino \textit{“partitionem”}, cioè divisione \cite{wordsense_partitionem:1}) principalmente per motivi di classificazione di dati e di sicurezza.
\\
Linux, così come UNIX, utilizza un sistema di cartelle gerarchico ad albero. A partire dalla cartella radice (\textit{“root”}, indicata con “/”)  troviamo, oltre che a possibili file, varie sotto cartelle che si presentano come foglie e che a loro volta possono contenere altri file o cartelle.
\\
Perché il contenuto di una partizione sia utilizzabile secondo questo sistema è necessario che venga \textit{“montato”} su di una cartella; viene così disposto all’interno di quest’ultima. La partizione principale viene montata sulla root e perciò ne prende il nome \cite{files_tldp:1}.
\\
Per quanto riguarda il partizionamento di Raspbian o più in generale dei sistemi per Raspberry Pi, solitamente troviamo due partizioni, quella di root e quella di \textit{“boot”}. Quest’ultima contiene tutto il necessario all’avvio del dispositivo come kernel, bootloader e file di configurazione ed essendo in formato FAT è facilmente accessibile anche da Microsoft Windows.

\subsubsection{File system}
In una partizione i dati sono contenuti sotto forma di una sequenza di blocchi di byte.
\\
Il filesystem si occupa di associare questi byte a file o cartelle, tenendo nota di alcune informazione come nome, data di creazione, dimensione e l’indirizzo fisico del disco dove risiedono i dati \cite{filesystem_tldp:1}.
\\
Alcuni di essi inoltre consentono l'assegnazione dei cosiddetti \textit{“extended attributes”}: dati nella forma di coppie chiave-valore \cite{man_attr:1}.
\\
Esistono numerosi filesystem diversi; per citarne alcuni: FAT e NTFS \cite{ms_ntfs_fat:1} sono ampiamente utilizzati in ambienti MS Windows, mentre per quanto riguarda Linux è opportuno menzionare ext4 \cite{ext4_kernel_org:1}.

\subsubsection{Tipi di File}
Nei sistemi UNIX il concetto di file è molto ampio: generalmente tutto ciò che vi si incontra può essere considerato un file, ad eccezione dei processi.
\\
Esistono infatti vari tipi di file oltre a quelli “regolari” e alle cartelle \cite{files_tldp:1}:
\begin{itemize}
	\item
	      Link: letteralmente collegamenti ad altri file,
	\item
	      Socket e Pipe: utilizzati per comunicazioni tra processi,
	\item
	      Device file: indicano generalmente periferiche hardware, come dischi rigidi o dispositivi di input \cite{devices_tldp:1}. Si dividono in:
	      \begin{itemize}
		      \item
		            Block: utilizzano comunicazioni bufferizzate (a blocchi),
		      \item
				Character: consentono una comunicazione diretta non bufferizzata.
		\end{itemize}
		Sono inoltre classificati da una coppia di numeri detti \textit{"device number"}.
\end{itemize}

\subsubsection{Linux Boot Process}
\begin{enumerate}
	\item \textbf{BIOS}
		Il primo elemento che prende parte all’avvio di un dispositivo dipende strettamente dalla piattaforma.
		\\
		Sui Personal Computer è detto BIOS (Basic Input/Output System) e si occupa di inizializzare l’hardware e di localizzare ed avviare il bootloader.
	\item \textbf{Bootloader}
	      É possibile che esista più di un bootloader e che vengano eseguiti uno dopo l’altro, con il compito di individuare la posizione del kernel e di caricarlo in memoria eventualmente insieme ad un \textit{"initrd"} oppure ad un \textit{"initramfs"}, rispettivamente un filesystem ed un archivio compressi che contengono un sistema minimale \cite{initramfs_kernel_org:1}.
	\item \textbf{Kernel}
	      Una volta eseguito, il kernel dopo operazioni necessarie all’hardware esegue un programma chiamato \textit{"init"} contenuto nel filesystem di root.
	\item \textbf{Initrd o Initramfs}
		Normalmente il kernel non conosce e non ha i mezzi per ricavare l'ubicazione della partizione di root.
		\\
		Per questo motivo viene utilizzato l’initrd o l’initramfs, precedentemente trasferito in memoria dal bootloader, come root temporanea.
		\\
		All'interno di esso è presente tutto ciò che occorre per localizzare e montare il vero filesystem di root, che effettivamente contiene il sistema, e successivamente di “scambiarlo” con quello attuale ed eseguirne l’init all’interno.
	\item \textbf{Init}
	      Quest'ultimo si occupa generalmente dell'inizializzazione di tutti i servizi necessari al sistema.
\end{enumerate}
~\\\\
I sistemi embedded presentano delle semplificazioni per il fatto che non incorrono nel problema di individuare la posizione di bootloader e kernel nei vari dischi.
\\
In aggiunta spesso i compiti di BIOS e bootloader sono svolti da un unico elemento e anche la partizione root può essere collocata in un sito prefissato e determinabile senza initrd/initramfs \cite{ibm_linux_boot:1}.

\subsection{Valutazioni}
Per questioni di probabilità di successo è utile disporre di una procedura in grado di ripristinare il sistema ad una versione funzionante.
\\
La sovrascrittura della microSD è in grado di assolvere il compito, ma presenta notevoli complessità perciò, come precedentemente accennato, è da considerare solo in casi estremi.

\subsubsection{Partizioni Speculari}
Una possibile tecnica è la creazione di due gruppi di partizioni, l’uno indipendente dall’altro, entrambi contenenti tutto il necessario per la gestione del sistema e di una piccola partizione FLAG in cui salvare quale dei due gruppi utilizzare. Ogni qualvolta venga effettuato l’aggiornamento viene sovrascritta la zona non utilizzata, il FLAG viene modificato ed è possibile avviare la versione più recente con la garanzia di disporre di un sistema funzionante accessibile semplicemente sovrascrivendo la FLAG (per esempio con un pulsante fisico).
Al momento dell’accensione è il bootloader stesso a leggere la FLAG e decidere quale kernel e quali partizioni utilizzare, in questo modo risulta l’unico elemento che accomuna i due sistemi.
\\
Per quanto riguarda lo spazio di archiviazione la richiesta è doppia rispetto ad una configurazione a sistema singolo \cite{artik_partitions:1}.
\\\\
Tuttavia il bootloader originale del Raspberry Pi non integra questa funzione e per motivi di licenza non è permesso modificarlo né visionarne il codice sorgente in quanto è distribuito in forma binaria \cite{broadcom_license:1}.
\\
È possibile un'implementazione simile attraverso un secondo bootloader che si occupa di scegliere quale sistema avviare.
Oppure con un unico kernel si è in grado di scegliere quale partizione di root utilizzare.
\\
Entrambe sono però implementazioni complicate che raddoppiano lo spazio necessario richiedendo che l’aggiornamento contenga la totalità del sistema, rendendo impossibili aggiornamenti incrementali.

\subsubsection{Overlay Filesystem}
Una soluzione alternativa che permette l’utilizzo di essi, oltre che di evitare il raddoppio dello spazio necessario è l’implementazione di un \textit{"Overlayfs"}, attraverso il modulo overlay del kernel.
\\
Ciò combina due filesystem o più in generale due \textit{“directory-tree”}, uno detto \textit{“lower”} ed uno \textit{“upper”}, la prima spesso in sola lettura (per questo chiamata anche rom: read only memory). Quando un file esiste in una sola delle due è mostrato semplicemente, in caso contrario viene utilizzato quello in upper. Per quanto riguarda cartelle presenti in ambedue i filesystem, queste vengono “unite”: appare una directory al cui interno troviamo il contenuto di entrambe (sovrapposto nel caso di omonimi).
\\
Le modifiche effettuate ad un Overlayfs sono scritte su upper, è possibile sovrascrivere oppure cancellare (apparentemente) file o cartelle presenti nel lower senza alterare quest'ultimo.
\\
La rimozione di elementi senza modificare il lower filesystem è possibile attraverso i cosiddetti \textit{“whiteout”} e le \textit{“opaque directory”}. I primi consistono in elementi character con numeri di device 0/0, mentre le seconde sono normali cartelle con l’attributo \textit{“trusted.overlay.opaque”} impostato a \textit{“y”}.
\\
Quando nella upper vi si incorre, nel primo caso viene nascosto l’omonimo contenuto in lower, nel secondo viene utilizzata la cartella in upper evitando di unirla alla corrispettiva in lower \cite{overlayfs_kernel_org:1}.
\\
Non modificare la lower directory rende possibile il ripristino ad una versione funzionante semplicemente eliminando il contenuto dell’upper.

\subsection{Implementazione dell'Overlay}
Purché la totalità del sistema sia aggiornabile limitando le modifiche ad upper è essenziale che l’overlay sia montata direttamente sulla root.
\\
A fianco delle due partizioni tuttora esistenti sulla microSD del CoderBot è opportuno aggiungerne una terza che conterrà l’upper filesystem dell’overlay, riservando alla precedente root il compito di lower filesystem.
\\
A questo punto è necessario che prima di eseguire le varie applicazioni vengano correttamente montate le varie partizioni necessarie.
\\
Sono due le possibili opzioni.

\subsubsection{Modifica di Initrd o Initramfs}
La prima è inserire le operazioni necessarie nell’initrd/initramfs in modo da montare correttamente l’overlay e successivamente eseguirne l’init.
\\
Tuttavia in genere l’initrd/initramfs viene modificato automaticamente ad ogni aggiornamento del kernel utilizzando software diversi in base alla distribuzione \cite{dracut:1}.

\subsubsection{Modifica dell'Init}
L’alternativa scelta per il CoderBot è di affidare il compito all’init stesso, in modo che sostituisca l’overlay all’attuale rootfs. In questo modo si ha a disposizione una procedura indipendente dal sistema utilizzato.

\paragraph{Boot Process Modificato}

Detto ciò il processo di boot del CoderBot richiede un passaggio aggiuntivo rispetto a quello generico di Linux.
\\
Una volta eseguito lo script di init modificato, questo deve svolgere i seguenti compiti:
\begin{enumerate}
	\item
	      Montare il filesystem di upper,
	\item
	      Caricare il modulo overlay del kernel,
	\item
	      Configurare correttamente l’overlay,
	\item
	      Sostituire la root, che attualmente consiste nel lower filesystem, con l’overlay tramite la procedura \textit{“pivot\_root”} \cite{man_pivot_root:1},
	\item
	      Eseguire l’init originale di Raspbian nel nuovo filesystem di root, cioè l’overlay, con il comando \textit{“chroot”} \cite{man_chroot:1}.
\end{enumerate}
\begin{figure}[h!]
	\centering
	\begin{verbatim}
	mount $upperdev /upper # monta upper
	modprobe overlay # carica il modulo overlay

	#monta l'overlay
	mount -t overlay overlay -o lowerdir=/,upperdir=/upper /new_root

	pivot_root /newroot /lower # /new_root e' la nuova root
	exec chroot /newroot /init # esegue l'init nella nuova root
	\end{verbatim}
	\caption{Procedimento dell'init modificato}
	\end{figure}
~\\
Nel caso in cui una o più di queste fasi fallissero, per esempio quando non esiste l’upper filesystem, è opportuno che lo script di init, oltre a registrarne i log, si occupi di eseguire il sistema utilizzando il lower filesystem come root in modo da garantire l’accessibilità ai servizi del CoderBot.

\subsubsection{Esecuzione del Reset}
L’esecuzione del reset, cioè la rimozione dei dati contenuti nell’upper filesystem, è problematica se questi sono in uso. È perciò opportuno che venga svolta prima che l’overlay sia montata.
\\
Questo comporta l’aggiunta di una fase nell’init modificato, che ancor prima di montare il filesystem di upper ha l’obbligo di controllare se l’utente intende effettuare il ripristino dei dati.
\\
In caso affermativo per motivi di efficienza si occupa di eliminare il filesystem corrente attraverso l’uso di \textit{“wipefs”} \cite{man_wipefs:1} e di crearne uno nuovo con \textit{“mkfs”} \cite{man_mkfs:1}, piuttosto che rimuovere i file singolarmente.

\paragraph{Partizione di boot e kernel}
Il kernel, risiedendo nella partizione di boot, non è ripristinato attraverso questa procedura, che pertanto non è soddisfacente per un corretto ripristino. In più eseguire una versione del kernel differente da quella corrispondente al sistema può causare problemi, soprattutto per quanto riguarda l’utilizzo di moduli sviluppati per un'altra versione.
\\
Per sopperire a questa mancanza è sufficiente eseguire preventivamente un backup degli elementi contenuti in boot e ripristinarli successivamente.
\\
Nel dettaglio ciò è gestito collocando il tutto in un archivio compresso (tar.xz) e ponendolo nel lower filesystem cosicché sia disponibile al momento opportuno.
\\
La garanzia che all’occorrenza di un reset il file di backup sia integro è ottenuta impedendone le modifiche ad utenti ordinari. L’utilizzo di firme digitali risulta inutile in quanto le chiavi sono modificabili in modo altrettanto facile dal superutente.

\subsubsection{Richiesta di Reset}
\paragraph{Interfaccia Grafica}
Per motivi di semplicità è opportuno che esista una procedura di ripristino effettuabile attraverso l’interfaccia grafica.
\\
Come accennato non è possibile eseguirlo mentre il sistema è attivo, tuttavia è sufficiente “annotarne” la volontà e riavviare il sistema cosicché l’init svolga il compito.
\\
Ciò è gestibile creando un semplice file di FLAG in una posizione concordata; una volta che l’init ne rileva l’esistenza concretizza il ripristino, dopo averlo ovviamente eliminato per evitare di ripetere l'operazione agli avvii successivi.

\paragraph{Metodi Alternativi}
Limitandosi ad una procedura basata sull’interfaccia, in caso quest’ultima non sia disponibile, per esempio in seguito a problemi con aggiornamenti oppure a modifiche dell’utente, risulterebbe impossibile accedere alla procedura di reset.
\\
È quindi evidente l’esigenza di metodi alternativi, nonostante non possano garantire uguale semplicità. Difatti una volta che appaiono problemi riguardo l’applicazione web, è impossibile non ricorrere all’accesso fisico del dispositivo.
\\
Ricordando che la sovrascrittura della microSD è effettivamente un ripristino ad una versione funzionante sono da considerare convenienti solo quelle procedure che rispetto ad essa presentano vantaggi in semplicità.

\subparagraph{File su MicroSD}
La prima alternativa consiste nel generare il file manualmente. Ciò richiede comunque l’estrazione della microSD dal Raspberry Pi e il suo collocamento in un Personal Computer, tuttavia non impone l’utilizzo di software appositi, ma soltanto la creazione di un nuovo file.

\subparagraph{Bottone di Reset}
Essendo il Raspberry Pi provvisto di un interfaccia GPIO, è possibile collegarvi un bottone.
\\
Gli utenti in possesso di un CoderBot che ne dispone sono provvisti di tutte le risorse necessarie per evitare l’accesso alla microSD, in quanto lo script di init ha la facoltà di verificarne lo stato durante l’avvio. Nel caso quest’ultimo risulti premuto effettua il reset.
\\
Ricordando che nei sistemi UNIX-like tutto ciò che non è un processo può essere considerato un file è intuibile che dal punto di vista dell'init la difficoltà di questa procedura sia comparabile con la semplice lettura di un file.
\\
Difatti per accedere allo stato di un pin dell’interfaccia GPIO è sufficiente utilizzare alcuni file contenuti in /sys, dove i sistemi UNIX e Linux sono soliti mantenere informazioni riguardanti vari dispositivi \cite{fhs:1}.

\subsection{Gestione dell'aggiornamento}

\subsubsection{Aggiornamento al Boot}
Per quanto riguarda l'aggiornamento è stata accennata la necessità di eseguirlo prima che l'applicazione CoderBot sia avviata. Questo è possibile affidando all'init modificato il compito di controllare la presenza di un pacchetto di aggiornamento in posizioni predefinite e  di eseguirlo.
\\
Perché le modifiche siano scritte su upper è necessario compiere questo passaggio dopo aver correttamente montato l'overlay sulla root.

\subsubsection{Ripristino dopo l'Aggiornamento}
Un ulteriore interrogativo da affrontare è come gestire il ripristino in seguito ad un aggiornamento.
\\
Sono due le principali possibilità da considerare:
\begin{itemize}
	\item Applicare semplicemente ogni aggiornamento sull'upper. In questo modo il ripristino riporta il sistema alla versione originale, presente nel lower filesystem sin dall'acquisto.
	
	\item Scrivere sul lower filesystem le modifiche finora apportate su upper, e dopo aver ripulito upper installarvi l'aggiornamento. Ciò permette il ripristino alla versione immediatamente precedente.
\end{itemize}
~\\
La scelta è ricaduta sulla seconda, in quanto meno invasiva dal punto di vista dell'utente.
Tuttavia la necessità di eseguire il reset dell'upper filesystem, dove probabilmente si trova il file di aggiornamento, obbliga a copiarlo preventivamente in una posizione di backup per poi applicarlo successivamente.