\section{Contesto}

\subsection{Robotica}
La robotica è la disciplina che si occupa di studio, realizzazione e utilizzo di robot, ossia macchine in grado di svolgere attività più o meno elementari interagendo con l’ambiente, sia a livello di percezione, attraverso vari sensori, che di azione. Le loro applicazioni variano dall’esecuzione di attività prestabilite, per esempio i robot costruiti per l’industria, fino allo sviluppo di capacità decisionali e cognitive.
\\
Negli ultimi decenni la popolarità di questa disciplina ha riscontrato un incremento sorprendente, dovuto al progresso riscontrato nei vari campi correlati con la robotica, come intelligenza artificiale, informatica o elettronica. I robot infatti, oltre che in ambito industriale, aerospaziale e militare sono sempre più integrati nella quotidianità.
\\
Parlando di domotica ci si riferisce all’automazione di varie attività relative alle abitazioni, come gestione di illuminazione, riscaldamento o pulizia \cite{treccani_matematica_robotica:1}.
Anche in campo automobilistico è possibile riscontrare una presenza sempre più importante di sistemi  in grado di svolgere varie azioni in autonomia, come parcheggiare, avvertire la necessità di una frenata di emergenza ed eseguirla, leggere cartelli stradali o perfino guidare autonomamente \cite{treccani_italiana_robotica:1}.
\\
Infine anche l’interesse nei benefici che la robotica può apportare all’educazione e all’apprendimento ha subito una crescita altrettanto repentina \cite{robo_edu:2}.

\subsubsection{Robotica Educativa}
La robotica educativa consiste in un approccio all’apprendimento pensato per essere semplice e divertente, appropriato a bambini e ragazzi di qualunque età e livello di preparazione.
Il suo utilizzo è principalmente orientato all’insegnamento di robotica e informatica, specialmente assemblando il robot stesso oppure programmandone le azioni. Ciò non ne esclude tuttavia un uso rivolto a discipline diverse come lingue, scienze, matematica o musica, dove viene tuttora impiegata. Per esempio alcuni studi hanno riscontrato risultati positivi nell'apprendimento linguistico combinando la robotica educativa all’uso di libri e registrazioni; a differenza di questi infatti i robot permettono approcci basati sull’interattività, fattore generalmente correlato all’interesse suscitato nei bambini.
\\
Da considerare inoltre è la versatilità della robotica nell’educazione, che può essere impiegata in modo più o meno attivo. Per esempio considerando l’assemblaggio di robot il ruolo affidato ad essi è pressoché nullo; diversamente accade quando il robot accompagna il bambino nel processo di apprendimento oppure quando ad esso è affidato interamente il compito di insegnante.
Un ulteriore vantaggio è che questi si dimostrano più adeguati allo svolgimento di attività ripetitive, rispetto ad interlocutori umani che potrebbero risultarne affaticati.
\\
Offrendo inoltre la possibilità di effettuare giochi con fini didattici oltre che di svago, la robotica educativa risulta appropriata ad attività ricreative ed extra-scolastiche.
\\
Tuttavia è necessario ricordare la presenza di limitazioni nelle attività che i robot sono in grado di svolgere. Per esempio è stata riscontrata, oltre ad essere facilmente comprensibile, la preferenza dei bambini all’ascolto di voci “umane”, piuttosto che meccaniche \cite{robo_edu:1}.

\subsection{CoderBot}
\begin{figure}[h!]
	\centering
	\includegraphics[
		height=5cm,
		keepaspectratio,
	]{CoderBot.png}
	\caption{L'aspetto di CoderBot.}
	\label{fig:blockly}
\end{figure}
~\\
CoderBot è un piccolo robot a batteria che si presenta come un veicolo a tre ruote dotato di vari dispositivi che ne permettono il movimento e l’interazione con l’ambiente circostante. É ad esempio in grado di evitare ostacoli, riconoscere oggetti e volti oppure emettere e ascoltare suoni.
\\
Rientra nell’ambito della robotica educativa, incentrandosi principalmente sull’insegnamento della programmazione che avviene pianificando le azioni del robot secondo modalità adeguate ad età e preparazione dell’interessato.
Esistono infatti due alternative: un’approccio più tradizionale basato sul linguaggio di programmazione Python, oppure l’utilizzo di Blockly (Figura \ref{fig:blockly}), che fornisce un’interfaccia per la programmazione grafica a blocchi utilizzabile anche da bambini sprovvisti di esperienza in questo campo.
\begin{figure}[h!]
	\centering
	\includegraphics[
		height=3cm,
		keepaspectratio,
	]{Blockly.png}
	\caption{L'interfaccia fornita da Blockly.}
	\label{fig:blockly}
\end{figure}
~\\
Infine CoderBot è un progetto libero, più precisamente è rilasciato sotto i termini della licenza GNU GPLv2 \cite{coderbotorg:1}.
\\
Per chiarire cosa ciò significhi è necessario introdurre alcuni concetti.

\paragraph{Licenza Software}
Con licenza software si indica un contratto tra il proprietario di un’applicazione e l’utente finale, che effettivamente non compra il software stesso ma la sua licenza di utilizzo. Questa definisce i diritti che l’utente può esercitare.
\\
Il fine è quello di tutelare il diritto d’autore sul proprio software, per esempio è possibile limitarne diffusione, installazione su più dispositivi, vietarne l’analisi del funzionamento e la successiva modifica per arginare la pirateria.
\\
Nel contratto vengono inoltre definite le responsabilità che il proprietario si assume nei confronti del software: spesso gli accordi contengono dichiarazioni volte a salvaguardare l’autore di fronte a conseguenze impreviste come problemi di sistema o hardware apparentemente correlati all’utilizzo del software in questione.
\\
Generalmente la richiesta di accettazione delle condizioni dell’accordo è effettuata durante il processo d’installazione, oppure può avvenire attraverso un documento apposito chiamato EULA (End User License Agreement).
\\
Una pratica comune soprattutto ai software \textit{“open source”}, che permettono di prendere visione dei codici sorgente, è collocare le clausole all’interno dei vari file \cite{what_is_software_licensing:1}.

\paragraph{Software Libero}
Spesso ci si riferisce al software libero con \textit{“open source”}, tuttavia ciò indica una filosofia diversa e concede minor evidenza al concetto di libertà.
\\
Infatti, secondo le direttive GNU, un software è definibile libero se concede le libertà ritenute fondamentali agli utenti, in modo che siano questi ad avere il controllo sul programma, ritenendo un abuso lasciarlo completamente agli sviluppatori.
\\
Inoltre il fatto che un software sia libero non ha ripercussioni sul prezzo con cui questo viene distribuito: è possibile che ciò avvenga gratuitamente così come in cambio di un compenso \cite{gnu_free_sw:1}.
\\
Un software libero garantisce:
\begin{enumerate}
	\item
	      La libertà che chiunque, persona o organizzazione, possa eseguire il programma per qualsiasi scopo ed in qualsiasi contesto, cosicché nessuno possa distribuire il software imponendo all’utente un determinato utilizzo.
	\item
	      La libertà di studiare e modificare il funzionamento del programma per adattarlo alle proprie esigenze. Ciò richiede necessariamente l’accesso al codice sorgente. Inoltre deve essere possibile l’utilizzo di versioni modificate del software.
	\item
	      La libertà di ridistribuire copie o versioni modificate a chiunque, sia in modo gratuito che retribuito, senza l'obbligo di chiederne o pagarne l’autorizzazione.
	\item
	      La libertà di distribuire pubblicamente e come software libero versioni modificate del programma, in modo tale da arricchire la comunità.
\end{enumerate}

\paragraph{GNU} È un progetto fondato nel 1983 da Richard M. Stallman che comprende vari programmi ed un sistema operativo che ne fa uso, rilasciati con licenze libere. 
\\
Il nome è l’acronimo ricorsivo di \textit{“GNU’s Not Unix”}, infatti nonostante siano apparentemente simili, GNU non utilizza parti di codice UNIX e nasce con lo scopo di crearne una valida alternativa che rispetti la libertà degli utenti. 
\\
La sua nascita coincide con quella del “movimento per il software libero” a supporto del quale nel 1985 lo stesso Stallman fondò la Free Software Foundation (FSF).
\\
Qualche anno più tardi nel 1989 venne inoltre stesa la prima versione della GNU General Public License (GPL), una delle più diffuse licenze per il software libero.
\\
Spesso invece che il kernel originale GNU viene utilizzato quello Linux. Ci si riferisce a ciò quando si parla di GNU/Linux \cite{gnu:1}.

\subsubsection{Hardware}
\paragraph{Raspberry Pi}
\begin{figure}[h!]
	\centering
	\includegraphics[
		height=5cm,
		keepaspectratio,
	]{Raspberry-Pi-3.jpg}
	\caption{Un Raspberry Pi 3 Model B.}
	\label{fig:pi3}
\end{figure}
Il CoderBot è costruito attorno ad un Raspberry Pi, precisamente alla versione \textit{“3 Model B”} (Figura \ref{fig:pi3}). Si tratta di un single-board computer dalle dimensioni di una carta di credito, economico e versatile oltre che efficiente per quanto riguarda il consumo energetico \cite{rpi_buy:1}. È infatti possibile alimentarlo a batteria (come effettivamente avviene per il CoderBot).
\\
Dispone di \cite{rpi_doc:1}:
\begin{itemize}
	\item
	      Una CPU ARM a 64bit prodotta da Broadcom,
	\item
	      1GB di memoria RAM,
	\item
	      Un’interfaccia di wireless LAN e Bluetooth Low Energy,
	\item
	      Un’interfaccia Ethernet,
	\item
	      Quattro porte USB2.0,
	\item
	      Una porta HDMI,
	\item
	      Un’interfaccia per la connessione a fotocamere,
	\item
	      Una per la connessione ad un display eventualmente touchscreen,
	\item
	      Una porta microSD,
	\item
	      Un’uscita audio analogica,
	\item
	      Un’interfaccia GPIO (General Purpose Input Output) che comprende 40 collegamenti.
\end{itemize}
~\\
È in grado di eseguire una grande varietà di sistemi operativi principalmente basati su GNU/Linux ed installati su microSD.

\paragraph{Periferiche}
Il restante hardware necessario al CoderBot comprende:
\begin{itemize}
	\item
	      Un microfono connesso attraverso una porta USB2.0,
	\item
	      Un altoparlante collegato all’uscita audio,
	\item
		Una fotocamera connessa all’apposita interfaccia,
	\item
	      Una scheda di controllo sviluppata appositamente per CoderBot allacciata alla GPIO che si occupa della gestione di:
	      \begin{itemize}
		      \item
		            Motori,
		      \item
		            Servo,
		      \item
		            Sensori di prossimità a ultrasuoni.
	      \end{itemize}
	      Mantenendo il tutto elettricamente isolato dal circuito principale del Raspberry Pi,
	\item
	      Una batteria.
\end{itemize}

\subsubsection{Software}

La programmazione del CoderBot è affidata ad un’architettura client-server composta totalmente da software libero.
\\

\begin{figure}[h!]
	\centering
	\framebox[\textwidth]{%
	\centering
		\begin{subfigure}[t]{0.5\textwidth}
			\dirtree{%
				.1 CoderBot.
				.2 Raspbian.
				.3 hostapd.
				.3 dnsmasq.
				.3 Python3 Backend App.
				.4 Flask.
				.5 Connexion.
				.6 API REST.
				.4 TensorFlow.
				.4 OpenCV.
				.4 pigpio module.
				.4 picamera.
				.4 PyAudio.
				.3 pigpio.
			}
		\end{subfigure}
		\begin{subfigure}[t]{0.5\textwidth}
			\dirtree{%
				.1 User Device.
				.2 Web Browser.
				.3 Vue.js Frontend App.
				.4 Vuetify.
				.4 Vuex.
				.4 Blockly.
				.4 AR.js.
				.4 Axios.
			}
		\end{subfigure}
	}
	\caption{Diagramma del Software}
\end{figure}

\paragraph{Frontend}
Il frontend consiste in un applicazione web che si occupa dell’intera gestione del robot da parte dell’utente.
\\
Attraverso di essa è infatti possibile programmare le azioni del robot scegliendo la modalità appropriata al proprio livello di esperienza, tra il linguaggio Python e l’interfaccia Blockly per la programmazione grafica a blocchi. Inoltre consente l’accesso alla pagina che permette l’utilizzo del dispositivo come veicolo radiocomandato accedendo in tempo reale a ciò che osserva e ascolta tramite fotocamera e microfono. Infine ne consente la configurazione mediante l’apposita sezione.
\\
Oltre alle funzioni considerabili “passive”, che svolgono solamente il compito di inviare e ricevere informazioni dal server, al client sono affidati ruoli “attivi”, ad esempio le operazioni necessarie alla conversione del programma Blockly in codice Python.
\\
La componente frontend utilizza principalmente Javascript, con il sostegno di alcune librerie e framework, cioè insiemi di librerie con il compito di fornire tutte le funzioni richieste da specifiche tipologie di applicazione \cite{doc_forge_framework:1}.
\\
Più precisamente:
\begin{itemize}
	\item
	      Javascript è un linguaggio di programmazione ad alto livello che rientra nei paradigmi di programmazione imperativo, funzionale, ad oggetti e ad eventi. È ampiamente utilizzato nell’ecosistema Web, principalmente lato client in modo da migliorare l’interattività delle pagine \cite{javascript:1}.
	\item
	      Vue.js è un framework Javascript per il Web orientato allo sviluppo di interfacce utente \cite{vuejs:1}. Su di esso si basa l’intera interfaccia del CoderBot.
	\item
		Vuetify consiste in un framework per Vue che fornisce alcune componenti per semplificare il processo di sviluppo dell'applicazione \cite{vuetify:1}.
	\item
		Vuex è una libreria per Vue che si occupa di gestire lo stato delle varie componenti dell'applicazione \cite{vuex:1}.
	\item
	      Blockly è un progetto sviluppato da Google Inc., consiste in una libreria Javascript che permette l’aggiunta alle pagine web di un editor per la programmazione grafica \cite{blockly:1}.
	\item
	      Axios, un client http sviluppato per Browser e Node.js, permette la comunicazione asincrona con il server in modo da realizzare applicazioni interattive \cite{axios:1}.
\end{itemize}

\paragraph{Backend}
Il backend del CoderBot è costituito da un’applicazione Python.
\\
In primo luogo ha il compito di comunicare con il frontend; ciò è affidato al modulo Flask che si occupa di eseguire il server HTTP (HyperText Transfer Protocol) necessario a rendere disponibile l’applicazione web, ovvero il frontend, a qualunque browser e ad esporre le API REST che permettono la comunicazione con il client.
\\
L'applicazione si occupa di eseguire il programma Python ottenuto dal frontend e di gestire l'interfaccia di controllo e comunicazione delle varie periferiche hardware tramite alcuni moduli come picamera, pigpio o PyAudio.
\\
Ad essa è inoltre affidata gran parte dell’elaborazione dei dati, come le operazioni necessarie al riconoscimento di oggetti o volti tramite TensorFlow e OpenCV.
\\
Nel dettaglio:
\begin{itemize}
	\item
		Python è un linguaggio di programmazione ad alto livello nato per essere semplice e intuibile, supporta diversi paradigmi di programmazione come quelli imperativo, funzionale e ad oggetti ed è utilizzato in svariati ambiti \cite{python:1}.
		\\
		CoderBot ne utilizza la versione 3.
	\item
		Flask è un framework Python per lo sviluppo di applicazioni web \cite{flask:main}. In realtà si definisce un \textit{“microframework”} \cite{flask:micro} per evidenziare la volontà di mantenersi semplice e facilmente espandibile lasciando allo sviluppatore la scelta dei software da integrare.
	\item 
		Connexion è un framework basato su Flask che permette di semplificare la creazione di API REST e la loro associazione a funzioni Python \cite{connexion:1}.
	\item
	      TensorFlow è una libreria ampiamente utilizzata negli ambiti di Machine Learning e Deep Learning, sviluppata dal team \textit{"Google Brain"} rivolgendo l’attenzione alla flessibilità che ne consente l’utilizzo su varie piattaforme oltre che alle performance \cite{tensorflow:1}.
	\item
	      OpenCV (Open Source Computer Vision Library) è una libreria che fornisce funzionalità di \textit{"Computer Vision"} nata pensando ad applicazioni in tempo reale, quindi ponendo importanza all’efficienza computazionale \cite{opencv:1}.
	\item
	      PyAudio consiste in una libreria Python che si occupa della gestione audio, sia per quando riguarda riprodurre che registrare suoni \cite{pyaudio:1}.
	\item
	      Picamera, come intuibile dal nome, è il modulo che permette l’utilizzo della fotocamera \cite{picamera:1}.
	\item
	      Infine pigpio è un modulo Python che permette l’interazione con l’omonimo servizio \cite{pigpio:python}.
\end{itemize}

\paragraph{REST}
Representational State Transfer è uno stile architetturale per sistemi ipermediali distribuiti, dove per \textit{“ipermedia”} si intende la rappresentazione di informazioni di varia natura come testi, immagini, video o collegamenti ipertestuali \cite{rest:1}.
\\
A seguire i principi guida di REST:
\begin{itemize}
	\item
	      Client-server: l’interfaccia utente rimane separata in modo da garantire migliore portabilità e semplificare la parte server.
	\item
	      Stateless: il server non conserva nessuno stato che riguardi il client: ogni richiesta deve contenere tutte le informazioni necessarie.
	\item
	      Cacheable: il client piuttosto che inviare nuovamente una richiesta ha la possibilità di utilizzare la risposta precedente se etichettata come \textit{“cacheable”}.
	\item
	      Uniform Interface: l’interfaccia di comunicazione tra client e server dev’essere uniforme e ben definita in modo da minimizzare la complessità dell’architettura.
	\item
	      Layered System: l’architettura presenta livelli gerarchici ai cui componenti non è permessa né necessaria l’interazione con altri strati.
	\item
	      Code on demand (opzionale): è possibile estendere le funzionalità del client scaricando codice in forma di applet o script.
\end{itemize}

\paragraph{Servizi}
Il funzionamento di CoderBot richiede inoltre alcuni servizi di sistema:
\begin{itemize}
	\item
	      Pigpio: un software con l'obiettivo di gestire la GPIO del Raspberry Pi \cite{pigpio:1}. È accessibile dal backend del CoderBot tramite l’omonimo modulo Python.
	\item
	      Hostapd: un servizio per la gestione di access point Wireless e della relativa autenticazione \cite{hostapd:1}; le funzionalità del CoderBot sono infatti accessibili connettendosi alla rete WiFi ad-hoc, sprovvista di connessione a internet, da esso gestita.
	\item
	      Dnsmasq: un software pensato per dispositivi dotati di risorse limitate che svolge il compito di DNS (Domain Name System) e DHCP (Dynamic Host Configuration Protocol) server \cite{dnsmasq:1}. Entrambi servizi utili per la connessione al CoderBot.
\end{itemize}

\paragraph{Raspbian}
È il sistema operativo scelto per ospitare l’intero stack del CoderBot. È basato su GNU/Linux e rilasciato secondo la licenza GNU GPL, così come il sistema da cui esso deriva, cioè Debian. Quest’ultimo è stato modificato e ottimizzato per l'esecuzione su Raspberry Pi \cite{raspbian:1}.

\subparagraph{Linux}
È un sistema operativo sviluppato a partire dai primi anni 90 da Linus Torvalds e distribuito nei termini della GNU General Public License \cite{linux_license:1}. Nato come clone di Unix, ne eredita lo standard POSIX (Portable Operating System Interface for UniX) e l'architettura monolitica attorno ad un kernel che contiene gran parte del software necessario alla gestione hardware \cite{linux_begin_unix_tldp:1}.
\\
È inoltre possibile estendere dinamicamente le funzionalità di quest'ultimo tramite i cosiddetti \textit{“moduli”} pensati per poter essere aggiunti e rimossi dal kernel quando richiesto \cite{modules_tldp:1}.

\subparagraph{UNIX}
Con il termine UNIX ci si riferisce ad una famiglia di sistemi operativi derivanti dall’originale sviluppato nei laboratori AT\&T nell’anno 1969.
\\
Ha riscontrato un'abbondante diffusione perché essendo scritto con linguaggi di alto livello ne risulta semplice l'adattamento a diverse architetture hardware.
\\
È organizzato attorno ad un programma di dimensioni notevoli che si occupa della gestione dell’hardware, chiamato kernel, permettendo così a gran parte del sistema di prescindere da quale sia il dispositivo utilizzato \cite{unix_kd_edu:1}.

